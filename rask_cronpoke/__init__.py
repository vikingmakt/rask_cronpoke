from datetime import datetime
from hashlib import sha1
from rask.base import Base
from rask.parser.json import dictfy
from rask.rmq import ack
from types import FunctionType

__all__ = ['Cronpoke']

class Cronpoke(Base):
    @property
    def __events__(self):
        try:
            assert self.__events
        except (AssertionError,AttributeError):
            self.__events = {
                'for_each_hour':[],
                'for_each_minute':[]
            }
        except:
            raise
        return self.__events

    @property
    def options(self):
        try:
            assert self.__options
        except (AssertionError,AttributeError):
            self.__options = {
                'actions':[
                    'for_each_hour',
                    'for_each_minute'
                ],
                'exchange':'cronpoke'
            }
        except:
            raise
        return self.__options

    @property
    def queue(self):
        try:
            assert self.__queue
        except (AssertionError,AttributeError):
            self.__queue = sha1('%s:%s' % (self.uuid,self.ioengine.uuid4)).hexdigest()
        except:
            raise
        return self.__queue

    def __declare__(self,channel):
        def on_declare(*args):
            for action in self.options['actions']:
                channel.queue_bind(
                    callback=None,
                    exchange=self.options['exchange'],
                    queue=self.queue,
                    routing_key='',
                    arguments={
                        'service':'cronpoke',
                        'action':action,
                        'x-match':'all'
                    }
                )

            channel.basic_consume(
                consumer_callback=self.__on_msg__,
                queue=self.queue
            )
            self.log.info('Listening')
            return True

        channel.queue_declare(
            callback=on_declare,
            auto_delete=True,
            exclusive=True,
            durable=False,
            queue=self.queue
        )
        return True

    def __init__(self,rmq):
        self.rmq = rmq
        self.ioengine.loop.add_callback(self.__services__)

    def __on_msg__(self,channel,method,properties,body):
        try:
            self.ioengine.loop.add_callback(
                self.__trigger_events__,
                cb=self.__events__[properties.headers['action']],
                date=datetime.strptime(dictfy(body)['date'],'%Y-%m-%dT%H:%M:%S.%f'),
                ack=self.ioengine.future(ack(channel,method))
            )
        except (KeyError,TypeError):
            ack(channel,method)(True)
        except:
            raise
        return True

    def __services__(self):
        def declare(_):
            self.ioengine.loop.add_callback(
                self.__declare__,
                channel=_.result().channel
            )
            return True

        self.rmq.channel('rask_cronpoke_%s' % self.uuid,self.ioengine.future(declare))
        return True

    def __trigger_events__(self,cb,date,ack,i=None):
        try:
            self.ioengine.loop.add_callback(i.next(),date)
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.__trigger_events__,
                cb=cb,
                date=date,
                ack=ack,
                i=iter(cb)
            )
        except StopIteration:
            ack.set_result(True)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.__trigger_events__,
                cb=cb,
                date=date,
                ack=ack,
                i=i
            )
        return True

    def add_cb_hour(self,cb):
        try:
            assert isinstance(cb,FunctionType)
        except AssertionError:
            raise
        except:
            raise
        else:
            self.__events__['for_each_hour'].append(cb)
        return True

    def add_cb_minute(self,cb):
        try:
            assert isinstance(cb,FunctionType)
        except AssertionError:
            raise
        except:
            raise
        else:
            self.__events__['for_each_minute'].append(cb)
        return True
